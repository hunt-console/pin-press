# pin-press

This is the instant-printer solution for the hunt-console rails application.

Package and create the JAR file for this project with:

```bash
mvn package
```

You may also want to clean the output directory with

```bash
mvn clean
```

# Setup

```bash
apt install openjdk-11-jdk maven
```

## Environment Variables

`JAVA_HOME` needs to be set. Usually this is done in the `~/.bashrc` initializer.
`JDBC_DATABASE_URL` needs to be set to the database URL containing the username and password. It's recommended
to create a `.env` file in the project directory (and it should be ignored by git) and to put this environment variable
in there. This should be of the format `postgres://{user}:{password}@{hostname}:{port}/{database-name}`

## Project Generation

This project was originally generated with this:

```bash
mvn archetype:generate -DgroupId=com.hunt_console.pin_press -DartifactId=pin-press -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
```
