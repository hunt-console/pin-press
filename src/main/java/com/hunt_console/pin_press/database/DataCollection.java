package com.hunt_console.pin_press.database;

import java.util.List;

import com.hunt_console.pin_press.models.LicensePrintout;

import org.springframework.lang.Nullable;

/**
 * Container class for the data returned from gathering database data.
 */
public class DataCollection {
    public final List<LicensePrintout> printouts;
    @Nullable
    public final Integer roundNumber;

    public DataCollection(List<LicensePrintout> printouts, Integer roundNumber) {
        this.printouts = printouts;
        this.roundNumber = roundNumber;
    }
}