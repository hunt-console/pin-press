package com.hunt_console.pin_press.database.collectors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Basic class for fetching the round number given its UUID.
 */
public class RoundNumberCollector {
    public static Integer collectRoundNumber(Connection database, UUID roundId) throws SQLException {
        PreparedStatement statement = database
                .prepareStatement("SELECT rounds.number FROM rounds WHERE rounds.id = ? LIMIT 1");
        statement.setObject(1, roundId);
        ResultSet results = statement.executeQuery();
        results.next();
        int value = results.getInt("number");
        return (value <= 0) ? null : value;
    }
}