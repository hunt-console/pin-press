package com.hunt_console.pin_press.database.collectors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.IntStream;

import com.hunt_console.pin_press.util.OrderDirection;
import com.hunt_console.pin_press.maps.LicensePrintoutMapper;
import com.hunt_console.pin_press.models.LicensePrintout;
import com.hunt_console.pin_press.util.ThrowingBiConsumer;

import org.springframework.util.StringUtils;

/**
 * Class for gathering printout data from a database.
 */
public class PrintoutCollector {
    private static final String QUERY_BASE = "WITH current_hunt_id AS (SELECT hunts.id FROM hunts INNER JOIN rounds ON "
            + "rounds.hunt_id = hunts.id AND rounds.id = ? LIMIT 1) " // Set round ID
            + "SELECT participants.first, participants.last, participants.extras, licenses.id "
            + "AS license_id, participants.id AS participant_id, RHS.match_id, RHS.match_number, RHS.first AS secondary_first, "
            + "RHS.last AS secondary_last, RHS.extras AS secondary_extras, RHS.license_id "
            + "AS secondary_license_id, RHS.participant_id AS secondary_participant_id "
            + "FROM participants INNER JOIN licenses ON licenses.participant_id = participants.id "
            + "INNER JOIN current_hunt_id ON current_hunt_id.id = licenses.hunt_id "
            + "LEFT OUTER JOIN licenses_matches AS lm ON licenses.id = lm.license_id "
            + "LEFT OUTER JOIN (SELECT matches.id AS match_id, matches.local_id AS match_number, participants.first, participants.last, participants.extras, "
            + "licenses.id AS license_id, participants.id AS participant_id, licenses.eliminated "
            + "FROM matches INNER JOIN licenses_matches AS lm "
            + "ON matches.id = lm.match_id INNER JOIN licenses ON licenses.id = lm.license_id "
            + "INNER JOIN current_hunt_id ON current_hunt_id.id = licenses.hunt_id "
            + "INNER JOIN participants ON participants.id = licenses.participant_id " + "WHERE matches.round_id = ?) " // Set
                                                                                                                       // round
                                                                                                                       // ID
            + "AS RHS ON lm.match_id = RHS.match_id AND RHS.participant_id <> participants.id "
            + "WHERE licenses.eliminated = false " + "AND (RHS.eliminated = false OR RHS.eliminated IS NULL)";
    private static final int QUERY_BASE_PARAM_COUNT = StringUtils.countOccurrencesOf(QUERY_BASE, "?");

    private static final String ORDER_DIRECTION_REPLACE = "%{order-direction}%";
    /**
     * The String to be reused when ordering by one of the participant extras.
     */
    private static final String ORDER_BY_BASE = String.format("CASE participants.extras->>? ~ '^[0-9]+$' "
            + "WHEN true THEN CAST(participants.extras->>? AS INT) ELSE NULL END %s, participants.extras->>? %s",
            ORDER_DIRECTION_REPLACE, ORDER_DIRECTION_REPLACE);
    private static final int ORDER_BY_PARAM_COUNT = StringUtils.countOccurrencesOf(ORDER_BY_BASE, "?");

    private List<ThrowingBiConsumer<PreparedStatement, Integer, SQLException>> parameters;
    private final Connection connection;
    private StringBuilder currentSql = new StringBuilder(QUERY_BASE);
    /**
     * Set to true when an order by clause has been specified in the data gathering,
     * else false.
     */
    private boolean haveOrdered = false;

    public PrintoutCollector(Connection databaseConnection, UUID roundId) {
        this.connection = databaseConnection;
        parameters = new ArrayList<>();
        ThrowingBiConsumer<PreparedStatement, Integer, SQLException> roundIdSetter = (statement, index) -> statement
                .setObject(index, roundId);
        addMultipleTimes(roundIdSetter, QUERY_BASE_PARAM_COUNT);
    }

    public PrintoutCollector orderBy(String extraPropertyKey, OrderDirection direction) {
        addMultipleTimes((statement, index) -> statement.setString(index, extraPropertyKey), ORDER_BY_PARAM_COUNT);
        if (!haveOrdered) {
            currentSql.append(" ORDER BY ");
        } else {
            currentSql.append(", ");
        }
        currentSql.append(ORDER_BY_BASE.replace(ORDER_DIRECTION_REPLACE, direction.getSql()));
        haveOrdered = true;
        return this;
    }

    private void addMultipleTimes(ThrowingBiConsumer<PreparedStatement, Integer, SQLException> consumer, int times) {
        IntStream.range(0, times).forEach((int ignored) -> parameters.add(consumer));
    }

    /**
     * Executes the built SQL query and returns the parsed objects.
     * 
     * @return The LicensePrintout objects built from the results of the SQL query.
     * @throws SQLException Thrown when there are issues with executing the SQL
     *                      statement.
     */
    public List<LicensePrintout> execute() throws SQLException {
        List<LicensePrintout> values = null;
        PreparedStatement statement = connection.prepareStatement(currentSql.toString());
        for (int i = 0; i < parameters.size(); i++) {
            parameters.get(i).accept(statement, i + 1);
        }
        ResultSet results = statement.executeQuery();
        LicensePrintoutMapper mapper = new LicensePrintoutMapper();
        values = new ArrayList<LicensePrintout>();
        while (results.next()) {
            values.add(mapper.mapRow(results));
        }
        return values;
    }
}