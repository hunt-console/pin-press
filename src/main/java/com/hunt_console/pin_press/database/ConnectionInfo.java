package com.hunt_console.pin_press.database;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

import com.beust.jcommander.internal.Nullable;

public class ConnectionInfo {
    private static final String DATABASE_URL_ENV_VAR = "DATABASE_URL", JDBC_DATABASE_URL_ENV_VAR = "JDBC_DATABASE_URL";
    private static final Logger LOGGER = Logger.getLogger(ConnectionInfo.class.getName());
    public final String url;
    @Nullable
    public final String username, password;

    /**
     * Instantiates a new ConnectionInfo object
     * 
     * @param url      The URL of the database.
     * @param username The username of the user to use when accessing the database.
     * @param password The password for the user accessing the database.
     */
    public ConnectionInfo(final String url, @Nullable final String username, @Nullable final String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * Instantiates a new ConnectionInfo object
     * 
     * @param url The URL of the database. With no username and password being
     *            provided, this must either contain the username and password, or
     *            the database must not require a username and password.
     */
    public ConnectionInfo(final String url) {
        this(url, null, null);
    }

    /**
     * Determines whether or not this connection info contains a username and
     * password.
     * 
     * @return True if the connection info has the database credentials (in which
     *         case they should be used), false otherwise (in which case, only the
     *         URL should be used).
     */
    public boolean hasUsernamePassword() {
        return username != null && password != null;
    }

    /**
     * Constructs a new ConnectionInfo instance from the DATABASE_URL environment
     * variable's value.
     * 
     * @return A new ConnectionInfo class containing the connection data.
     * @throws URISyntaxException
     */
    public static ConnectionInfo fromDatabaseUrl() throws URISyntaxException {
        ConnectionInfo connectionInfo = null;
        String jdbcDatabaseUrl = System.getenv(JDBC_DATABASE_URL_ENV_VAR);
        String databaseUrl = System.getenv(DATABASE_URL_ENV_VAR);
        if (jdbcDatabaseUrl != null) {
            connectionInfo = new ConnectionInfo(jdbcDatabaseUrl);
        } else if (databaseUrl != null) {
            URI databaseURI = new URI(databaseUrl);
            String[] userInfo = databaseURI.getUserInfo().split(":");
            String username = userInfo[0];
            String password = userInfo[1];
            connectionInfo = new ConnectionInfo("jdbc:postgresql://" + databaseURI.getHost() + ":"
                    + databaseURI.getPort() + databaseURI.getPath() + "?sslmode=require", username, password);
        } else {
            LOGGER.severe("fromDatabaseUrl() called with no \"" + JDBC_DATABASE_URL_ENV_VAR + "\" or \""
                    + DATABASE_URL_ENV_VAR + "\" environment variable set! One of those must be set.");
        }
        if (databaseUrl == null) {
        }
        return connectionInfo;
    }
}