package com.hunt_console.pin_press.database;

import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import com.hunt_console.pin_press.database.collectors.PrintoutCollector;
import com.hunt_console.pin_press.database.collectors.RoundNumberCollector;
import com.hunt_console.pin_press.models.LicensePrintout;
import com.hunt_console.pin_press.util.FieldOrdering;
import com.hunt_console.pin_press.util.StacktraceToString;

/**
 * Class that handles connecting to the database and gathering necessary data
 * from it.
 */
public class DataCollector {
    private static final Logger LOGGER = Logger.getLogger(DataCollector.class.getName());

    private final DatabaseHandler connector;

    public DataCollector() throws Exception {
        try {
            this.connector = new DatabaseHandler();
        } catch (URISyntaxException | SQLException e) {
            LOGGER.severe("Unable to connect to the database.");
            LOGGER.severe(StacktraceToString.stacktraceToString((e)));
            throw e;
        }
    }

    /**
     * Closes the database connection.
     * 
     * @throws SQLException
     */
    public void closeConnection() throws SQLException {
        this.connector.connection.close();
    }

    /**
     * Gathers the required data from the configured database.
     * 
     * @param roundId        The round ID for which to gather data.
     * @param fieldOrderings The orderings for the license printouts.
     * @return The collection of data gathered from the database.
     * @throws SQLException
     */
    public DataCollection execute(final UUID roundId, final FieldOrdering[] fieldOrderings) throws SQLException {
        PrintoutCollector gatherer = new PrintoutCollector(this.connector.connection, roundId);
        for (FieldOrdering ordering : fieldOrderings) {
            gatherer.orderBy(ordering.orderedProperty, ordering.direction);
        }
        List<LicensePrintout> printouts = null;
        Integer roundNumber = null;
        try {
            printouts = gatherer.execute();
            roundNumber = RoundNumberCollector.collectRoundNumber(this.connector.connection, roundId);
        } catch (SQLException e) {
            LOGGER.severe("Problem executing query.");
            LOGGER.severe(StacktraceToString.stacktraceToString(e));
            throw e;
        }
        return new DataCollection(printouts, roundNumber);
    }
}