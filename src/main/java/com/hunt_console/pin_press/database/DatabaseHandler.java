package com.hunt_console.pin_press.database;

import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.beust.jcommander.internal.Nullable;

import org.springframework.lang.NonNull;

public class DatabaseHandler {
    public final Connection connection;

    /**
     * Constructs a new DatabaseHandler from the
     * 
     * @throws URISyntaxException
     * @throws SQLException
     */
    public DatabaseHandler() throws URISyntaxException, SQLException {
        this(ConnectionInfo.fromDatabaseUrl());
    }

    /**
     * @param databaseUrl The JDBC database URL to connect to.
     * @param username    User's username.
     * @param password    User's password.
     * @throws SQLException
     */
    public DatabaseHandler(@NonNull String databaseUrl, @Nullable String username, @Nullable String password)
            throws SQLException {
        this.connection = (username != null && password != null)
                ? DriverManager.getConnection(databaseUrl, username, password)
                : DriverManager.getConnection(databaseUrl);
    }

    /**
     * @param info Information required to connect to the database.
     * @throws SQLException
     */
    public DatabaseHandler(ConnectionInfo info) throws SQLException {
        this(info.url, info.username, info.password);
    }
}