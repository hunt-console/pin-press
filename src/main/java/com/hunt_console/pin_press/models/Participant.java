package com.hunt_console.pin_press.models;

import java.util.Map;
import java.util.UUID;

/**
 * Participant model class, containing only the information that we will be
 * using for participants.
 */
public class Participant extends Record {
    public final String first, last;
    public final Map<String, String> extras;

    public Participant(UUID id, String first, String last, Map<String, String> extras) {
        super(id);
        this.first = first;
        this.last = last;
        this.extras = extras;
    }
}