package com.hunt_console.pin_press.models;

import java.util.Map;
import java.util.UUID;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Class for keeping track of all information required to print a license to
 * PDF.
 */
public class LicensePrintout {
    @NonNull
    private final Participant primary;
    @Nullable
    private final Participant secondary;
    @Nullable
    private final Integer matchNumber;

    /**
     * Constructs a license printout given its two containing participants.
     * 
     * @param primary   The primary participant.
     * @param secondary The secondary participant.
     * @param matchNumber   The ID of the match in which the primary and secondary
     *                  participant are competing. Would be null if there is no
     *                  secondary participant.
     */
    public LicensePrintout(Participant primary, @Nullable Participant secondary, @Nullable Integer matchNumber) {
        this.primary = primary;
        this.secondary = secondary;
        this.matchNumber = matchNumber;
    }

    /**
     * Constructs a new license printout given the data for the two participants it
     * contains.
     * 
     * @param primaryId       The primary participant's ID.
     * @param primaryFirst    The primary participant's first name.
     * @param primaryLast     The primary participant's last name.
     * @param primaryExtras   The extras property for the primary participant.
     * @param secondaryId     The secondary participant's ID.
     * @param secondaryFirst  The secondary participant's first name.
     * @param secondaryLast   The secondary participant's last name.
     * @param secondaryExtras The extras property for the secondary participant.
     * @param matchNumber         The ID of the match in which the primary and secondary
     *                        participant are competing. Would be null if there is
     *                        no secondary participant.
     */
    public LicensePrintout(UUID primaryId, String primaryFirst, String primaryLast, Map<String, String> primaryExtras,
            UUID secondaryId, String secondaryFirst, String secondaryLast, Map<String, String> secondaryExtras,
            @Nullable Integer matchNumber) {
        this.primary = new Participant(primaryId, primaryFirst, primaryLast, primaryExtras);
        this.secondary = new Participant(secondaryId, secondaryFirst, secondaryLast, secondaryExtras);
        this.matchNumber = matchNumber;
    }

    public Participant getPrimary() {
        return primary;
    }

    public Participant getSecondary() {
        return secondary;
    }

    public Integer getMatchId() {
        return matchNumber;
    }
}