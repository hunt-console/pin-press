package com.hunt_console.pin_press.models;

import java.util.UUID;

public abstract class Record {
    protected UUID id;

    public Record() {
    }

    public Record(UUID id) {
        this.id = id;
    }

    public UUID getUuid() {
        return id;
    }
}