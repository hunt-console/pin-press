package com.hunt_console.pin_press.printer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hunt_console.pin_press.models.LicensePrintout;
import com.hunt_console.pin_press.printer.fillers.Filler;

import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

/**
 * Class responsible for printing a list of LicensePrintouts.
 */
public class Printer {
    protected final File outputPDF;
    protected final TemplatePDF inputPDF;
    protected final ByteArrayInputStream copiedInputPDF;

    private List<Filler> fillers = new ArrayList<>();

    /**
     * @param inputPDF  The input PDF file (template PDF)
     * @param outputPDF The file at which to output the results.
     * @throws IOException when there is a problem opening or loading files.
     */
    public Printer(File inputPDF, File outputPDF) throws IOException {
        this.inputPDF = new TemplatePDF(inputPDF);
        this.outputPDF = outputPDF;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        this.inputPDF.document.save(output);
        this.copiedInputPDF = new ByteArrayInputStream(output.toByteArray());
    }

    /**
     * Adds a filler to map from a license printout to filling in the value of a
     * form field. Note that the order of adding these does not matter.
     * 
     * @param filler The filler to be added.
     */
    public void addFiller(Filler filler) {
        this.fillers.add(filler);
    }

    /**
     * Prints the licenses for this printer.
     * 
     * @param licenses The license records to be printed. It is assumed that they
     *                 should be printed in the order given.
     * @throws IOException
     */
    public void print(List<LicensePrintout> licenses) throws IOException {
        // Copy so as to not modify the original.
        ArrayList<LicensePrintout> printoutsCopy = new ArrayList<>(licenses);
        PDFMergerUtility merger = new PDFMergerUtility();
        final int fieldsPerTemplate = this.inputPDF.getNumEntries();
        // back
        while (!printoutsCopy.isEmpty()) {
            merger.addSource(new ByteArrayInputStream(copyAndFill(printoutsCopy, fieldsPerTemplate)));
        }
        // duplicateSufficientPages(licenses, outputPDF);
        // populateFormData(licenses, outputPDF);
        merger.setDestinationFileName(this.outputPDF.getAbsolutePath());
        merger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
    }

    /**
     * Copies the template PDF and fill it with the given number of licenses from
     * the given array. Removes elements that have been filled onto the PDF.
     * 
     * @param licenses  The array of licenses from which to fill. Filled licenses
     *                  will be removed from this array.
     * @param numToFill The number of licenses to fill from the array.
     * @return A byte array of the PDF file filled with the data.
     * @throws IOException
     */
    private byte[] copyAndFill(List<LicensePrintout> licenses, final int numToFill) throws IOException {
        PDDocument documentCopy = copyInputPDF();
        fillFields(documentCopy, licenses, numToFill);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        documentCopy.save(outputStream);
        documentCopy.close();
        return outputStream.toByteArray();
    }

    /**
     * Copies the input PDF file object.
     * 
     * @return A deep copy of the input PDF file.
     * @throws IOException
     */
    private PDDocument copyInputPDF() throws IOException {
        copiedInputPDF.reset();
        return PDDocument.load(copiedInputPDF);
    }

    /**
     * Fills all fields in the given PDF document with the information from the
     * given licenses. Flattens the filled fields as well.
     * 
     * @param document  The document to fill from.
     * @param licenses  The licenses to fill in the information for.
     * @param numFields The number of fields in the given document that can be
     *                  filled with license information.
     * @throws IOException
     */
    private void fillFields(PDDocument document, List<LicensePrintout> licenses, final int numToFill)
            throws IOException {
        fillFields(document, licenses, true);
    }

    /**
     * Fills all fields in the given PDF document with the information from the
     * given licenses.
     * 
     * {@link #fillFields(PDDocument, List, TemplatePDF, boolean)
     * Printer.fillFields} method
     * 
     * @param document The document to fill from.
     * @param licenses The licenses to fill in the information for.
     * @param flatten  True to flatten the fields after filling, false otherwise.
     * @throws IOException
     */
    private void fillFields(PDDocument document, List<LicensePrintout> licenses, final boolean flatten)
            throws IOException {
        fillFields(document, licenses, inputPDF, flatten, Collections.unmodifiableList(this.fillers));
    }

    /**
     * Fills all fields in the given PDF document with the information from the
     * given licenses.
     * 
     * @param document The document to fill from.
     * @param licenses The licenses to fill in the information for.
     * @param inputPDF The input template PDF. Used when determining the number of
     *                 fields that can be filled.
     * @param flatten  True to flatten the fields after filling, false otherwise.
     * @throws IOException
     */
    private static void fillFields(PDDocument document, List<LicensePrintout> licenses, final TemplatePDF inputPDF,
            final boolean flatten, List<Filler> fillers) throws IOException {
        PDAcroForm form = document.getDocumentCatalog().getAcroForm();
        // Update all the fillers with the information we have so far.
        fillers.forEach((filler) -> {
            filler.setDocument(document);
            filler.setForm(form);
        });
        ArrayList<PDField> fields = new ArrayList<>();
        for (int i = 1; i <= inputPDF.getNumEntries() && !licenses.isEmpty(); i++) {
            LicensePrintout currentPrintout = licenses.get(0);

            for (Filler filler : fillers) {
                PDField field = filler.fillFor(currentPrintout, i);
                if (field != null) {
                    fields.add(field);
                }
            }

            // End stuff.
            licenses.remove(0);
        }
        form.flatten(fields, true);
    }

    /**
     * Closes the printer. Must be called once everything is done.
     * 
     * @throws IOException If there is a problem closing the printer.
     */
    public void close() throws IOException {
        this.inputPDF.document.close();
    }
}