package com.hunt_console.pin_press.printer;

import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.hunt_console.pin_press.models.Participant;

/**
 * Utility class for mapping a participant's data to a string entry for a field.
 */
public class FieldMap {
    public static final String FIRST_KEY = "first", LAST_KEY = "last";

    private final String formatString;

    public FieldMap(final String formatString) {
        this.formatString = formatString;
    }

    public String getFormatString() {
        return formatString;
    }

    /**
     * Applies this print format to the given license to render what should be
     * printed to the field.
     * 
     * @param license The license to be rendered.
     * @return The string value of what should be printed for the license.
     */
    public String apply(final Participant data) {
        String value = new String(formatString);
        value = value.replace(toFormatString(FIRST_KEY), data.first);
        value = value.replace(toFormatString(LAST_KEY), data.last);
        for (Entry<String, String> entry : data.extras.entrySet()) {
            value = value.replaceAll(toExtrasRegex(entry.getKey()), entry.getValue());
        }
        return value;
    }

    /**
     * Constructs the format string which should be replaced with a participant
     * field value.
     * 
     * @param key The key that will be replaced.
     * @return A string to be replaced by the data object's value.
     */
    private static String toFormatString(final String key) {
        return "${" + key + "}";
    }

    /**
     * Formats a regular expression string built to replace extras property field
     * names with the data value.
     * 
     * @param key The extras property key.
     * @return A regular expression string that matches any extra property
     *         declaration for this property's key.
     */
    private static String toExtrasRegex(final String key) {
        return "(?i)" + Pattern.quote(toFormatString("#" + key));
    }
}