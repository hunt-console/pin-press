package com.hunt_console.pin_press.printer;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import com.beust.jcommander.internal.Nullable;
import com.hunt_console.pin_press.database.DataCollection;
import com.hunt_console.pin_press.printer.fillers.MessageFiller;
import com.hunt_console.pin_press.printer.fillers.NoFillFormatException;
import com.hunt_console.pin_press.printer.fillers.PrimaryFiller;
import com.hunt_console.pin_press.printer.fillers.QRFiller;
import com.hunt_console.pin_press.printer.fillers.RoundFiller;
import com.hunt_console.pin_press.printer.fillers.SecondaryFiller;

/**
 * Wrapper class for the generic Printer class that is specific to printing the
 * common license printout.
 */
public class CommonPrinter extends Printer {
    private static final Logger LOGGER = Logger.getLogger(CommonPrinter.class.getName());

    /**
     * 
     * @param inputPdf  The input (template) PDF file.
     * @param outputPdf The file to which to output the PDF file.
     * @param data      The loaded license printout data.
     * @throws IOException
     * @throws NoFillFormatException
     */
    public CommonPrinter(File inputPdf, File outputPdf, DataCollection data) throws IOException, NoFillFormatException {
        this(inputPdf, outputPdf, data, null, null);
    }

    /**
     * 
     * @param inputPdf  The input (template) PDF file.
     * @param outputPdf The file to which to output the PDF file.
     * @param data      The loaded license printout data.
     * @param fillText  The user-inputted fill text string. Can be null, in which
     *                  case no QR codes will be printed.
     * @param message   The message to be filled in to the message field on each
     *                  printout.
     * @throws IOException
     * @throws NoFillFormatException
     */
    public CommonPrinter(File inputPdf, File outputPdf, DataCollection data, @Nullable String fillText,
            @Nullable String message) throws IOException, NoFillFormatException {
        super(inputPdf, outputPdf);
        addFiller(new PrimaryFiller(this.inputPDF));
        try {

            addFiller(new SecondaryFiller(this.inputPDF));
        } catch (NoFillFormatException e) {
            LOGGER.warning("No secondary participant fill format provided. Skipping filling secondary participant...");
        }
        if (data.roundNumber != null) {
            addFiller(new RoundFiller(data.roundNumber));
        }
        if (fillText != null) {
            addFiller(new QRFiller(fillText));
        }
        if (message != null) {
            addFiller(new MessageFiller(message));
        }
    }
}