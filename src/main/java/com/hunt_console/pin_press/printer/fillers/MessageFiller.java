package com.hunt_console.pin_press.printer.fillers;

import com.hunt_console.pin_press.printer.TemplatePDF;

import org.springframework.lang.NonNull;

public class MessageFiller extends TextFiller {

    public MessageFiller(@NonNull final String message) {
        if (message == null) {
            throw new IllegalArgumentException("Message cannot be null.");
        }
        setValueGetter((printout) -> message);
        setFieldNameSupplier(TemplatePDF::getMessageFieldName);
    }
}