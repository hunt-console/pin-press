package com.hunt_console.pin_press.printer.fillers;

import java.util.function.Function;

import com.hunt_console.pin_press.models.LicensePrintout;
import com.hunt_console.pin_press.models.Participant;
import com.hunt_console.pin_press.printer.FieldMap;
import com.hunt_console.pin_press.printer.TemplatePDF;

import org.apache.pdfbox.pdmodel.interactive.form.PDField;

public class ParticipantFiller extends TextFiller {

    private static final String NULL_VALUE_STRING = "Move on";

    private final FieldMap fieldMap;
    private final Function<LicensePrintout, Participant> printoutToParticipant;

    public ParticipantFiller(TemplatePDF template, Function<Integer, String> getFieldName,
            Function<LicensePrintout, Participant> printoutToParticipant) throws NoFillFormatException {
        this.printoutToParticipant = printoutToParticipant;

        final String expectedFieldName = getFieldName.apply(1);
        final PDField formatField = template.document.getDocumentCatalog().getAcroForm().getField(expectedFieldName);
        if (formatField == null) {
            throw new NoFillFormatException("Missing a field data format from " + expectedFieldName + ".");
        }
        // Grab the format for filling in participant information from the first field.
        this.fieldMap = new FieldMap(formatField.getValueAsString());

        setValueGetter(this::printoutToString);
        setFieldNameSupplier(getFieldName);
    }

    private String printoutToString(LicensePrintout printout) {
        Participant participant = printoutToParticipant.apply(printout);
        return (participant == null) ? NULL_VALUE_STRING : fieldMap.apply(participant);
    }
}