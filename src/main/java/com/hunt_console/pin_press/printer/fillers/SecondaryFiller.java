package com.hunt_console.pin_press.printer.fillers;

import com.hunt_console.pin_press.models.LicensePrintout;
import com.hunt_console.pin_press.printer.TemplatePDF;

public class SecondaryFiller extends ParticipantFiller {
    public SecondaryFiller(TemplatePDF template) throws NoFillFormatException {
        super(template, TemplatePDF::getSecondaryFieldName, (LicensePrintout printout) -> printout.getSecondary());
    }
}