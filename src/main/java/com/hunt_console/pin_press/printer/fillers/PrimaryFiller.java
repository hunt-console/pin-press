package com.hunt_console.pin_press.printer.fillers;

import com.hunt_console.pin_press.models.LicensePrintout;
import com.hunt_console.pin_press.printer.TemplatePDF;

public class PrimaryFiller extends ParticipantFiller {
    public PrimaryFiller(TemplatePDF template) throws NoFillFormatException {
        super(template, TemplatePDF::getPrimaryFieldName, (LicensePrintout printout) -> printout.getPrimary());
    }
}