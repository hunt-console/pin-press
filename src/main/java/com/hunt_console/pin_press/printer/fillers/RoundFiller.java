package com.hunt_console.pin_press.printer.fillers;

import com.hunt_console.pin_press.printer.TemplatePDF;

/**
 * Filler for round fields.
 */
public class RoundFiller extends TextFiller {
    /**
     * Constructs a RoundFiller for the given round number.
     * 
     * @param roundNumber The number to be inputted into the round field.
     */
    public RoundFiller(int roundNumber) {
        String roundString = String.valueOf(roundNumber);
        setValueGetter((printout) -> roundString);
        setFieldNameSupplier(TemplatePDF::getRoundFieldName);
    }
}