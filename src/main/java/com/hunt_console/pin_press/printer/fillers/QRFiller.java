package com.hunt_console.pin_press.printer.fillers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.hunt_console.pin_press.models.LicensePrintout;
import com.hunt_console.pin_press.printer.TemplatePDF;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceDictionary;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceStream;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

/**
 * Filler class for generating QR codes on a given field.
 */
public class QRFiller extends DocumentFiller {
    private static final Logger LOGGER = Logger.getLogger(QRFiller.class.getName());

    private final String urlFormattable;

    /**
     * 
     * @param urlFormattable A valid URL containing a formattable %s to be filled in
     *                       with a match UUID in order to generate a URL to the
     *                       view for the match described by a particular printout.
     */
    public QRFiller(final String urlFormattable) {
        this.urlFormattable = urlFormattable;
    }

    @Override
    public PDField fillFor(LicensePrintout printout, int fieldNumber) throws IOException {
        final PDField field = form.getField(TemplatePDF.getQRFieldName(fieldNumber));
        final Integer matchNumber = printout.getMatchId();
        if (field != null && matchNumber != null) {
            fill(matchNumber, field);
        }
        return field;
    }

    private void fill(final Integer matchId, PDField field) throws IOException {
        PDRectangle rectangle = getRectangle(field);
        final int height = (int) rectangle.getHeight(); // Cast should round down.
        final int width = (int) rectangle.getWidth();

        String url = String.format(urlFormattable, matchId.toString());
        byte[] imageOutput = generateQRCode(url, width, height);

        if (imageOutput != null) {
            List<PDAnnotationWidget> widgets = field.getWidgets();
            if (widgets != null && widgets.size() > 0) {
                PDAnnotationWidget annotationWidget = widgets.get(0); // Only care about the first widget.

                PDImageXObject image = PDImageXObject.createFromByteArray(document, imageOutput, null);

                PDAppearanceStream pdAppearanceStream = new PDAppearanceStream(document);
                pdAppearanceStream.setResources(new PDResources());
                try (PDPageContentStream pdPageContentStream = new PDPageContentStream(document, pdAppearanceStream)) {
                    // Position will end up being relative to the widget.
                    pdPageContentStream.drawImage(image, 0, 0, width, height);
                }
                pdAppearanceStream.setBBox(new PDRectangle(0, 0, width, height));

                PDAppearanceDictionary pdAppearanceDictionary = annotationWidget.getAppearance();
                if (pdAppearanceDictionary == null) {
                    pdAppearanceDictionary = new PDAppearanceDictionary();
                    annotationWidget.setAppearance(pdAppearanceDictionary);
                }

                pdAppearanceDictionary.setNormalAppearance(pdAppearanceStream);
            }
        }
    }

    private static PDRectangle getRectangle(PDField field) {
        COSDictionary fieldDict = field.getCOSObject();
        COSArray fieldAreaArray = (COSArray) fieldDict.getDictionaryObject(COSName.RECT);
        return new PDRectangle(fieldAreaArray);
    }

    /**
     * Generates a QR code with the passed in properties.
     * 
     * @param url    The URL to which the QR code shall point.
     * @param width  The width for the QR code.
     * @param height The height for the QR code.
     * @return A byte array containing the encoded image of the QR code.
     * @throws IOException
     */
    private static byte[] generateQRCode(final String url, final int width, final int height) throws IOException {
        ByteArrayOutputStream outputStream = null;
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix matrix = writer.encode(url, BarcodeFormat.QR_CODE, width, height);
            outputStream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(matrix, "PNG", outputStream);

        } catch (WriterException e) {
            LOGGER.warning("Failed to generate QR code for URL " + url);
        }
        return (outputStream == null) ? null : outputStream.toByteArray();
    }

}