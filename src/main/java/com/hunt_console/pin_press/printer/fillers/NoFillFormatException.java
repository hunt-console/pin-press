package com.hunt_console.pin_press.printer.fillers;

/**
 * Exception thrown when there is a missing fill format for one of the
 * participant fields.
 */
public class NoFillFormatException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NoFillFormatException(String errorMessage) {
        super(errorMessage);
    }

}