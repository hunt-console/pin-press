package com.hunt_console.pin_press.printer.fillers;

import org.apache.pdfbox.pdmodel.PDDocument;

/**
 * A filler which needs access to both the document object and the form object.
 */
public abstract class DocumentFiller extends FieldFiller {
    protected PDDocument document;

    @Override
    public void setDocument(PDDocument document) {
        this.document = document;
    }
}