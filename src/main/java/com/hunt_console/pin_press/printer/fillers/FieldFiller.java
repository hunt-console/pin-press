package com.hunt_console.pin_press.printer.fillers;

import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;

/**
 * A class representing a field filler - that is, a filler which needs access to
 * a form object.
 */
public abstract class FieldFiller implements Filler {
    protected PDAcroForm form;

    @Override
    public void setForm(PDAcroForm form) {
        this.form = form;
    }
}