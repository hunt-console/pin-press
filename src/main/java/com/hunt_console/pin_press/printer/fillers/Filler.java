package com.hunt_console.pin_press.printer.fillers;

import java.io.IOException;

import com.hunt_console.pin_press.models.LicensePrintout;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

public interface Filler {
    public PDField fillFor(LicensePrintout printout, final int fieldNumber) throws IOException;

    public default void setDocument(PDDocument document) {
    }

    public default void setForm(PDAcroForm form) {
    }
}