package com.hunt_console.pin_press.printer.fillers;

import java.io.IOException;
import java.util.function.Function;

import com.hunt_console.pin_press.models.LicensePrintout;

import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

/**
 * A filler that fills in text fields based on the printout data alone. This is
 * a common operation, so nice to have a class for it.
 */
public class TextFiller extends FieldFiller {
    private Function<LicensePrintout, String> valueGetter;
    private Function<Integer, String> fieldNameSupplier;

    /**
     * 
     * @param valueGetter A function that takes in a license printout and maps it to
     *                    the value to be inputted to the form field.
     * 
     */
    protected void setValueGetter(Function<LicensePrintout, String> valueGetter) {
        this.valueGetter = valueGetter;
    }

    /**
     * @param fieldNameSupplier A supplier of the expected name of the field.
     */
    protected void setFieldNameSupplier(Function<Integer, String> fieldNameSupplier) {
        this.fieldNameSupplier = fieldNameSupplier;
    }

    @Override
    public PDField fillFor(LicensePrintout printout, int fieldNumber) throws IOException {
        PDField field = form.getField(fieldNameSupplier.apply(fieldNumber));
        // Instanceof should check not null here too.
        if (field instanceof PDTextField) {
            field.setValue(valueGetter.apply(printout));
        }
        return field;
    }

}