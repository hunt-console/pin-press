package com.hunt_console.pin_press.printer;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

/**
 * Represents a TemplatePDF object.
 */
public class TemplatePDF {
    public static final String PRIMARY_FIELD_NAME_FORMATTABLE = "p#%d";
    public static final String SECONDARY_FIELD_NAME_FORMATTABLE = "s#%d";
    public static final String ROUND_FIELD_NAME_FORMATTABLE = "r#%d";
    public static final String QR_FIELD_NAME_FORMATTABLE = "qr#%d";
    public static final String MESSAGE_FIELD_NAME_FORMATTABLE = "m#%d";
    public static final String PROPERTY_MATCH_REGEX = "#[1-9]+$";
    public static final Pattern PRIMARY_PROPERTY_NAME_PATTERN = Pattern.compile("^p" + PROPERTY_MATCH_REGEX);

    /** Number of form field entries in the template PDF. Value is lazy-loaded. */
    private int numEntries = -1;
    public final PDDocument document;
    private FieldMap primaryFieldMap = null, secondaryFieldMap = null;

    /**
     * Constructs a TemplatePDF object using the string path at which the file is
     * located.
     * 
     * @param filePath The path at which the TemplatePDF file is located.
     * @throws IOException If there are issues loading the file.
     */
    public TemplatePDF(final String filePath) throws IOException {
        this(new File(filePath));
    }

    /**
     * @param file The file from which to load the template PDF.
     * @throws IOException Throws if there is trouble loading the file.
     */
    public TemplatePDF(final File file) throws IOException {
        this(PDDocument.load(file));
    }

    /**
     * Constructs a TemplatePDF object from a PDF Document.
     * 
     * @param pdf The PDF document from which to create the TemplatePDF file.
     */
    public TemplatePDF(final PDDocument pdf) {
        this.document = pdf;
    }

    /**
     * Determines the number of unique entries possible to be inserted on this
     * template PDF file.
     * 
     * @return The number of licenses that can be printed in a single segment of the
     *         template PDF file.
     */
    public int getNumEntries() {
        if (numEntries < 0) {
            this.numEntries = computeNumEntries(this.document);
        }
        return numEntries;
    }

    public FieldMap getPrimaryFieldMap() {
        if (primaryFieldMap == null) {
            primaryFieldMap = new FieldMap(
                    document.getDocumentCatalog().getAcroForm().getField(getPrimaryFieldName(1)).getValueAsString());
        }
        return primaryFieldMap;
    }

    public FieldMap getSecondaryFieldMap() {
        if (secondaryFieldMap == null) {
            secondaryFieldMap = new FieldMap(
                    document.getDocumentCatalog().getAcroForm().getField(getSecondaryFieldName(1)).getValueAsString());
        }
        return secondaryFieldMap;
    }

    /**
     * Computes the number of PDF field entries in the PDF Document.
     * 
     * @param document The document on which to compute.
     * @return The number of entries
     */
    private static int computeNumEntries(final PDDocument document) {
        int count = 0;
        PDDocumentCatalog catalog = document.getDocumentCatalog();
        PDAcroForm form = catalog.getAcroForm();
        for (PDField field : form.getFields()) {
            final String fieldName = field.getPartialName();
            // If the primary license field name is present, increment the count by 1.
            if (field instanceof PDTextField && PRIMARY_PROPERTY_NAME_PATTERN.matcher(fieldName).matches()) {
                count++;
            }
        }
        return count;
    }

    /**
     * Gets the expected primary field name given the number it should have on the
     * page.
     * 
     * @param number The number of the field on the page. Indices start at 1.
     * @return The expected string name for the field.
     */
    public static String getPrimaryFieldName(final int number) {
        return String.format(PRIMARY_FIELD_NAME_FORMATTABLE, number);
    }

    /**
     * Gets the expected secondary field name given the number it should have on the
     * page.
     * 
     * @param number The number of the field on the page. Indices start at 1.
     * @return The expected string name for the field.
     */
    public static String getSecondaryFieldName(final int number) {
        return String.format(SECONDARY_FIELD_NAME_FORMATTABLE, number);
    }

    /**
     * Gets the expected field name for the round field given the number.
     * 
     * @param number The number of the field on the page to obtain.
     * @return The expected field name.
     */
    public static String getRoundFieldName(int number) {
        return String.format(ROUND_FIELD_NAME_FORMATTABLE, number);
    }

    /**
     * Gets the expected field name for the QR code field for the given number.
     * 
     * @param number The number for the field.
     * @return The expected field name.
     */
    public static String getQRFieldName(int number) {
        return String.format(QR_FIELD_NAME_FORMATTABLE, number);
    }

    /**
     * Gets the expected field name for teh message field for teh given number.
     * 
     * @param fieldNumber The number for the field relative to the other fields in
     *                    the template PDF.
     * @return The expected field name.
     */
    public static String getMessageFieldName(int fieldNumber) {
        return String.format(MESSAGE_FIELD_NAME_FORMATTABLE, fieldNumber);
    }
}