package com.hunt_console.pin_press;

import java.io.File;
import java.util.UUID;
import java.util.concurrent.Callable;

import com.hunt_console.pin_press.util.FieldOrdering;

import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

/**
 * Argument parser for the application.
 */
public class PressArgs implements Callable<PressArgs> {

    @Parameters(index = "0", paramLabel = "ROUND_ID", description = "UUID of the round for which to print licenses.")
    private UUID roundId;

    @Option(names = { "-t", "--template" }, description = "Path to the template PDF file.", required = true)
    private File templatePDF;

    @Option(names = { "-o", "--output" }, description = "Path to the output file.", required = true)
    private File outputPDF;

    @Option(names = { "-d", "--ordering" }, description = "Ordering for the licenses to be printed in."
            + " Should have format of \"<property name>:[asc|desc]\"."
            + "Property name needs to match casing exactly with the database.")
    private FieldOrdering[] orderings;

    @Option(names = { "-f",
            "--fill" }, description = "A string with which to fill in a match UUID and generate a QR code for."
                    + " Should be of the form \"match ID: %s now some other text\" which would replace the %s with the match UUI and print a QR code"
                    + " that contains the resultant string.")
    private String fillText;

    @Option(names = { "-m", "--message" }, description = "A custom user message to be filled in every message field.")
    private String message;

    @Override
    public PressArgs call() {
        return this;
    }

    /**
     * Gets the inputted round id.
     * 
     * @return The inputted round id.
     */
    public UUID getRoundId() {
        return roundId;
    }

    /**
     * Gets the inputted template PDF file.
     * 
     * @return The file object representing the template PDF.
     */
    public File getTemplatePDF() {
        return templatePDF;
    }

    /**
     * Gets the output PDF file.
     * 
     * @return The file at which the output should be saved.
     */
    public File getOutputPDF() {
        return outputPDF;
    }

    /**
     * Gets the user-requested field orderings.
     * 
     * @return The field orderings requested by the user, represented as
     *         FieldOrdering objects.
     */
    public FieldOrdering[] getFieldOrderings() {
        return orderings == null ? new FieldOrdering[] {} : orderings;
    }

    /**
     * Gets the user inputted fill text.
     * 
     * @return The fill text inputted by the user, or null if no fill text was
     *         inputted.
     */
    public String getFillText() {
        return fillText;
    }

    /**
     * Gets the user inputted message.
     * 
     * @return The user inputted message.
     */
    public String getMessage() {
        return message;
    }
}