SELECT matches.id AS match_id, licenses.id AS license_id, participants.id AS 
participant_id, licenses.eliminated FROM matches INNER JOIN licenses_matches 
AS lm ON matches.id = lm.match_id INNER JOIN licenses ON licenses.id = lm.license_id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a'
INNER JOIN participants ON participants.id = licenses.participant_id

-- First working iteration
SELECT participants.id AS participant_id, licenses.eliminated, RHS.* 
FROM participants INNER JOIN licenses ON licenses.participant_id = participants.id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a' 
LEFT OUTER JOIN licenses_matches AS lm ON licenses.id = lm.license_id 
LEFT OUTER JOIN (SELECT matches.id AS match_id, licenses.id AS license_id, participants.id AS 
participant_id, licenses.eliminated FROM matches INNER JOIN licenses_matches 
AS lm ON matches.id = lm.match_id INNER JOIN licenses ON licenses.id = lm.license_id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a' 
INNER JOIN participants ON participants.id = licenses.participant_id) 
AS RHS ON lm.match_id = RHS.match_id AND RHS.participant_id <> participants.id 
WHERE licenses.eliminated = false AND (RHS.eliminated = false OR RHS.eliminated IS NULL);

-- Working copy 1
SELECT licenses.id as license_id, participants.id AS participant_id, licenses.eliminated, RHS.* 
FROM participants INNER JOIN licenses ON licenses.participant_id = participants.id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a' 
LEFT OUTER JOIN licenses_matches AS lm ON licenses.id = lm.license_id 
LEFT OUTER JOIN (SELECT matches.id AS match_id, licenses.id AS license_id, participants.id AS 
participant_id, licenses.eliminated FROM matches INNER JOIN licenses_matches 
AS lm ON matches.id = lm.match_id INNER JOIN licenses ON licenses.id = lm.license_id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a' 
INNER JOIN participants ON participants.id = licenses.participant_id) 
AS RHS ON lm.match_id = RHS.match_id AND RHS.participant_id <> participants.id 
WHERE licenses.eliminated = false AND (RHS.eliminated = false OR RHS.eliminated IS NULL);

-- Working copy 2 (adding round filter on matches)
SELECT licenses.id as license_id, participants.id AS participant_id, licenses.eliminated, RHS.* 
FROM participants INNER JOIN licenses ON licenses.participant_id = participants.id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a' 
LEFT OUTER JOIN licenses_matches AS lm ON licenses.id = lm.license_id 
LEFT OUTER JOIN (SELECT matches.id AS match_id, licenses.id AS license_id, participants.id AS 
participant_id, licenses.eliminated FROM matches INNER JOIN licenses_matches 
AS lm ON matches.id = lm.match_id INNER JOIN licenses ON licenses.id = lm.license_id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a' 
INNER JOIN participants ON participants.id = licenses.participant_id
INNER JOIN rounds ON rounds.id = matches.round_id AND rounds.number = 2) 
AS RHS ON lm.match_id = RHS.match_id AND RHS.participant_id <> participants.id 
WHERE licenses.eliminated = false 
AND (RHS.eliminated = false OR RHS.eliminated IS NULL);

-- Working copy 3 (showing desired columns only)
SELECT participants.first, participants.last, participants.extras, licenses.id 
AS license_id, participants.id AS participant_id, RHS.match_id, RHS.first AS secondary_first, 
RHS.last AS secondary_last, RHS.extras AS secondary_extras, RHS.license_id 
AS secondary_license_id, RHS.participant_id AS secondary_participant_id
FROM participants INNER JOIN licenses ON licenses.participant_id = participants.id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a' 
LEFT OUTER JOIN licenses_matches AS lm ON licenses.id = lm.license_id 
LEFT OUTER JOIN (SELECT matches.id AS match_id, participants.first, participants.last, participants.extras, 
licenses.id AS license_id, participants.id AS participant_id, licenses.eliminated 
FROM matches INNER JOIN licenses_matches AS lm 
ON matches.id = lm.match_id INNER JOIN licenses ON licenses.id = lm.license_id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a' 
INNER JOIN participants ON participants.id = licenses.participant_id
INNER JOIN rounds ON rounds.id = matches.round_id AND rounds.number = 2) 
AS RHS ON lm.match_id = RHS.match_id AND RHS.participant_id <> participants.id 
WHERE licenses.eliminated = false 
AND (RHS.eliminated = false OR RHS.eliminated IS NULL);

-- Working copy 4 (casting columns to integer)
SELECT participants.first, participants.last, participants.extras, licenses.id 
AS license_id, participants.id AS participant_id, RHS.match_id, RHS.first AS secondary_first, 
RHS.last AS secondary_last, RHS.extras AS secondary_extras, RHS.license_id 
AS secondary_license_id, RHS.participant_id AS secondary_participant_id
FROM participants INNER JOIN licenses ON licenses.participant_id = participants.id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a' 
LEFT OUTER JOIN licenses_matches AS lm ON licenses.id = lm.license_id 
LEFT OUTER JOIN (SELECT matches.id AS match_id, participants.first, participants.last, participants.extras, 
licenses.id AS license_id, participants.id AS participant_id, licenses.eliminated 
FROM matches INNER JOIN licenses_matches AS lm 
ON matches.id = lm.match_id INNER JOIN licenses ON licenses.id = lm.license_id 
AND licenses.hunt_id = 'ff115129-0cb2-476c-8f10-6bff48ef962a' 
INNER JOIN participants ON participants.id = licenses.participant_id
INNER JOIN rounds ON rounds.id = matches.round_id AND rounds.number = 2) 
AS RHS ON lm.match_id = RHS.match_id AND RHS.participant_id <> participants.id 
WHERE licenses.eliminated = false 
AND (RHS.eliminated = false OR RHS.eliminated IS NULL)
ORDER BY CASE participants.extras->>'number' ~ '^[0-9]+$'
    WHEN true THEN CAST(participants.extras->>'number' AS INT)
    ELSE NULL
END DESC;

-- Working copy 5 (using round ID only)
WITH current_hunt_id AS (SELECT hunts.id FROM hunts INNER JOIN rounds ON
rounds.hunt_id = hunts.id AND rounds.id = '1a0eb09c-20c5-43e4-9070-b5be13e6c433' LIMIT 1)
SELECT participants.first, participants.last, participants.extras, licenses.id 
AS license_id, participants.id AS participant_id, RHS.match_id, RHS.first AS secondary_first, 
RHS.last AS secondary_last, RHS.extras AS secondary_extras, RHS.license_id 
AS secondary_license_id, RHS.participant_id AS secondary_participant_id
FROM participants INNER JOIN licenses ON licenses.participant_id = participants.id 
INNER JOIN current_hunt_id ON current_hunt_id.id = licenses.hunt_id
LEFT OUTER JOIN licenses_matches AS lm ON licenses.id = lm.license_id 
LEFT OUTER JOIN (SELECT matches.id AS match_id, participants.first, participants.last, participants.extras, 
licenses.id AS license_id, participants.id AS participant_id, licenses.eliminated 
FROM matches INNER JOIN licenses_matches AS lm 
ON matches.id = lm.match_id INNER JOIN licenses ON licenses.id = lm.license_id 
INNER JOIN current_hunt_id ON current_hunt_id.id = licenses.hunt_id
INNER JOIN participants ON participants.id = licenses.participant_id
WHERE matches.round_id = '1a0eb09c-20c5-43e4-9070-b5be13e6c433') 
AS RHS ON lm.match_id = RHS.match_id AND RHS.participant_id <> participants.id 
WHERE licenses.eliminated = false 
AND (RHS.eliminated = false OR RHS.eliminated IS NULL)
ORDER BY CASE participants.extras->>'number' ~ '^[0-9]+$'
    WHEN true THEN CAST(participants.extras->>'number' AS INT)
    ELSE NULL
END DESC;

-- Side work on casting display
SELECT participants.first, participants.last, participants.extras,
CASE participants.extras->>'number' ~ '^[0-9]+$'
    WHEN true THEN CAST(participants.extras->>'number' AS INT)
    ELSE NULL
END AS number
FROM participants WHERE participants.roster_id = 'd48911da-b354-4056-b9ba-24e22bccfcb4'
ORDER BY number DESC;

-- Side work on casting just order
SELECT participants.first, participants.last, participants.extras
FROM participants WHERE participants.roster_id = 'd48911da-b354-4056-b9ba-24e22bccfcb4'
ORDER BY CASE participants.extras->>'number' ~ '^[0-9]+$'
    WHEN true THEN CAST(participants.extras->>'number' AS INT)
    ELSE NULL
END DESC;


SELECT participants.first, participants.last, CAST(participants.extras->>'number' AS int)
FROM participants WHERE participants.roster_id = 'd48911da-b354-4056-b9ba-24e22bccfcb4';

-- Query that gets the hunt ID given the round ID
SELECT hunts.id FROM hunts INNER JOIN rounds ON rounds.hunt_id = hunts.id 
AND rounds.id = '1a0eb09c-20c5-43e4-9070-b5be13e6c433' LIMIT 1;

-- Working on WITH __ AS subquery common table expression
WITH current_hunt_id AS (SELECT hunts.id FROM hunts INNER JOIN rounds ON
rounds.hunt_id = hunts.id AND rounds.id = '1a0eb09c-20c5-43e4-9070-b5be13e6c433' LIMIT 1)
SELECT current_hunt_id.id FROM current_hunt_id;

-- Working copy 5 (using round ID only)
WITH current_hunt_id AS (SELECT hunts.id FROM hunts INNER JOIN rounds ON
rounds.hunt_id = hunts.id AND rounds.id = '1a0eb09c-20c5-43e4-9070-b5be13e6c433' LIMIT 1)
SELECT participants.first, participants.last, participants.extras
FROM participants INNER JOIN licenses ON licenses.participant_id = participants.id 
INNER JOIN current_hunt_id ON current_hunt_id.id = licenses.hunt_id;