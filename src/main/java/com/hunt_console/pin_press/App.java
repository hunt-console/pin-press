package com.hunt_console.pin_press;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.hunt_console.pin_press.database.DataCollection;
import com.hunt_console.pin_press.database.DataCollector;
import com.hunt_console.pin_press.printer.CommonPrinter;
import com.hunt_console.pin_press.printer.Printer;
import com.hunt_console.pin_press.printer.fillers.NoFillFormatException;
import com.hunt_console.pin_press.util.FieldOrdering;
import com.hunt_console.pin_press.util.FieldOrderingConverter;

import org.apache.pdfbox.pdmodel.PDDocument;

import picocli.CommandLine;

/**
 * Entry point class for the main pin press application.
 */
public class App {
    private static final Logger LOGGER = setupLogger();
    public static final String LOGGING_DIRECTORY = "pin-press-logs";

    public static void main(String[] args) throws Exception {
        CommandLine commandLine = new CommandLine(new PressArgs());
        commandLine.registerConverter(FieldOrdering.class, new FieldOrderingConverter());
        int exitCode = commandLine.execute(args);
        if (exitCode != CommandLine.ExitCode.OK) {
            System.exit(exitCode);
        }
        run(commandLine.getExecutionResult());
    }

    public static void run(PressArgs args) throws Exception {
        File templatePdf = args.getTemplatePDF();
        if (!templatePdf.exists()) {
            final String errorMessage = "Template PDF at path " + templatePdf.getAbsolutePath() + " doesn't exist!";
            LOGGER.severe(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        DataCollector collector = new DataCollector();
        DataCollection data = collector.execute(args.getRoundId(), args.getFieldOrderings());
        collector.closeConnection();

        try {
            Printer printer = new CommonPrinter(templatePdf, args.getOutputPDF(), data, args.getFillText(),
                    args.getMessage());
            printer.print(data.printouts);
            printer.close();
        } catch (NoFillFormatException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    public static void create() throws IOException {
        PDDocument document = new PDDocument();
        document.save("test.pdf");
        document.close();
    }

    private static Logger setupLogger() {
        // Prepare log directory
        File logDirectory = new File(LOGGING_DIRECTORY);
        if (!logDirectory.exists()) {
            logDirectory.mkdir();
        }

        Logger logger = Logger.getLogger(App.class.getPackageName());
        logger.setLevel(Level.INFO);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String currentTime = dateTimeFormatter.format(LocalDateTime.now());
        try {
            FileHandler handler = new FileHandler(Paths.get(LOGGING_DIRECTORY, currentTime + ".log").toString());
            handler.setLevel(Level.INFO);
            handler.setFormatter(new SimpleFormatter());
            logger.addHandler(handler);
        } catch (SecurityException | IOException e) {
            System.err.println("Failed to create log file.");
            e.printStackTrace();
        }

        return logger;
    }
}
