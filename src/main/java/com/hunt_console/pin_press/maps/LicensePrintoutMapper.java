package com.hunt_console.pin_press.maps;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hunt_console.pin_press.models.LicensePrintout;
import com.hunt_console.pin_press.models.Participant;

public class LicensePrintoutMapper implements RowMapper<LicensePrintout> {
    private static final String PRIMARY_PARTICIPANT_ID_COL = "participant_id", PRIMARY_PARTICIPANT_FIRST_COL = "first",
            PRIMARY_PARTICIPANT_LAST_COL = "last", PRIMARY_PARTICIPANT_EXTRAS_COL = "extras",
            SECONDARY_PARTICIPANT_ID_COL = "secondary_participant_id",
            SECONDARY_PARTICIPANT_FIRST_COL = "secondary_first", SECONDARY_PARTICIPANT_LAST_COL = "secondary_last",
            SECONDARY_PARTICIPANT_EXTRAS_COL = "secondary_extras", MATCH_NUMBER_COL = "match_number";

    @Override
    public LicensePrintout mapRow(ResultSet result) throws SQLException {
        LicensePrintout printout = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            // Primary participant
            Map<String, String> extras = mapper.readValue(result.getString(PRIMARY_PARTICIPANT_EXTRAS_COL),
                    new TypeReference<Map<String, String>>() {
                    });
            Participant primary = new Participant(result.getObject(PRIMARY_PARTICIPANT_ID_COL, UUID.class),
                    result.getString(PRIMARY_PARTICIPANT_FIRST_COL), result.getString(PRIMARY_PARTICIPANT_LAST_COL),
                    extras);

            // Secondary participant
            UUID secondaryId = result.getObject(SECONDARY_PARTICIPANT_ID_COL, UUID.class);
            Participant secondary = null;
            if (secondaryId != null) {
                Map<String, String> secondaryExtras = mapper.readValue(
                        result.getString(SECONDARY_PARTICIPANT_EXTRAS_COL), new TypeReference<Map<String, String>>() {
                        });
                secondary = new Participant(secondaryId, result.getString(SECONDARY_PARTICIPANT_FIRST_COL),
                        result.getString(SECONDARY_PARTICIPANT_LAST_COL), secondaryExtras);
            }

            // Match information
            Integer matchNumber = result.getInt(MATCH_NUMBER_COL);
            if (matchNumber <= 0) {
                matchNumber = null;
            }
            printout = new LicensePrintout(primary, secondary, matchNumber);
        } catch (JsonProcessingException e) {
            System.err.println("Error converting extras to Map<String, String>!");
            e.printStackTrace();
        }
        return printout;
    }

}