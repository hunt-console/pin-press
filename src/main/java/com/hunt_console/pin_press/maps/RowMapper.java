package com.hunt_console.pin_press.maps;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Generic interface for instantiating an object from a query row.
 * 
 * @param <T> The class of the object to be instantiated.
 */
public interface RowMapper<T> {
    public T mapRow(ResultSet results) throws SQLException;
}