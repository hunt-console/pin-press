package com.hunt_console.pin_press.util;

@FunctionalInterface
public interface ThrowingBiConsumer<T, U, E extends Exception> {
    public void accept(T first, U second) throws E;
}