package com.hunt_console.pin_press.util;

import picocli.CommandLine.ITypeConverter;
import picocli.CommandLine.TypeConversionException;

public class FieldOrderingConverter implements ITypeConverter<FieldOrdering> {

    @Override
    public FieldOrdering convert(String value) throws Exception {
        String[] values = value.split(":");
        if (values.length != 2) {
            throw new TypeConversionException("Invalid ordering: must be <property name>:<direction>");
        }
        return new FieldOrdering(values[0], values[1]);
    }

}