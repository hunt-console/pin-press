package com.hunt_console.pin_press.util;

public enum OrderDirection {
    Ascending("ASC"), Descending("DESC");

    private final String sql;

    private OrderDirection(String sqlEquivalent) {
        this.sql = sqlEquivalent;
    }

    public String getSql() {
        return this.sql;
    }

    /**
     * Converts a string order command to an order direction.
     * 
     * @param direction The direction string. Must be one of "ASC", "DESC", case
     *                  insensitive.
     * @return The OrderDirection equivalent for the given string.
     */
    public static OrderDirection fromString(String direction) {
        OrderDirection value;
        if (direction.equalsIgnoreCase("asc")) {
            value = OrderDirection.Ascending;
        } else if (direction.equalsIgnoreCase("desc")) {
            value = OrderDirection.Descending;
        } else {
            throw new IllegalArgumentException("The provided direction string does not match an OrderDirection.");
        }
        return value;
    }
}