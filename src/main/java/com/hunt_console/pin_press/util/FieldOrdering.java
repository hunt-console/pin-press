package com.hunt_console.pin_press.util;

/**
 * Class for storing data about a field ordering.
 */
public class FieldOrdering {
    public final String orderedProperty;
    public final OrderDirection direction;

    /**
     * Constructs a field ordering from the ordered property name and the order
     * direction.
     * 
     * @param orderedProperty The property name for the ordered property.
     * @param direction       The sort direction.
     */
    public FieldOrdering(final String orderedProperty, OrderDirection direction) {
        this.orderedProperty = orderedProperty;
        this.direction = direction;
    }

    /**
     * Constructs a field ordering from the ordered property name and the string
     * direction.
     * 
     * @param orderedProperty The property name for the ordered property.
     * @param direction       The sort direction string.
     */
    public FieldOrdering(final String orderedProperty, String direction) {
        this(orderedProperty, OrderDirection.fromString(direction));
    }
}