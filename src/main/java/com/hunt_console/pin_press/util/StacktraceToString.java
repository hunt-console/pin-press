package com.hunt_console.pin_press.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class StacktraceToString {
    /**
     * Converts a stacktrace from an exception to a string.
     * 
     * @param e The exception from which to gather the stacktrace.
     */
    public static String stacktraceToString(Exception e) {
        StringWriter writer = new StringWriter();
        PrintWriter printer = new PrintWriter(writer);
        e.printStackTrace(printer);
        return writer.toString();
    }
}